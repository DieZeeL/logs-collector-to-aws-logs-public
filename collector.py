#!/usr/bin/python
# coding=utf-8
import sys
import time
import os

import json
import traceback

import sqlite3

import boto
import boto.logs
import boto.logs.exceptions
import boto.exception
from cdaemon import Daemon

__author__ = 'DieZ'

debug = False
config = dict(region="us-east-1",
              aws_access_key_id="",
              aws_secret_access_key="",
              fail_count=5,
              timestamp="",
              group="",
              stream="",
              message="",
              microtime=long(round(time.time() * 1000)),
              self_group="AmazonCollector",
              self_args_stream="args",
              self_error_stream="errors")

class Logger(object):
    def __init__(self, filename="Default.log"):
        """

        :rtype : object
        """
        self.log = open(filename, "a")

    def write(self, message):
        self.log.write(message)

sys.stdout = Logger("/var/log/collector.log")
sys.stderr = Logger("/var/log/collector-err.log")

class Worker():
    def __init__(self):
        global config, debug
        self.config = config
        self.debug = debug
        self.row = dict(
            timestamp="",
            group="",
            stream="",
            message="")
        self.logs = boto.logs.connect_to_region(self.config['region'],
                                                aws_access_key_id=self.config['aws_access_key_id'],
                                                aws_secret_access_key=self.config['aws_secret_access_key'])

    @staticmethod
    def sleep_time(fail):
        return 2 * (int(fail) + 1)

    def to_log(self, fail=0):
        if self.debug:
            print "to_log(): execute check_group_stream() with variables:\n"
            print self.row['group'], self.row['stream'], "\n"
        self.check_group_stream()
        try:
            if self.debug:
                print "to_log(): compile message:\n"
                print "to_log(): type of message is ", self.row['message'], "\n"
            if isinstance(self.row['message'], (list, tuple, dict)):
                if self.debug:
                    print "to_log(): if message is json object - serialize it:\n"
                self.row['message'] = json.dumps(self.row['message'])
            mes = dict(timestamp=self.row['timestamp'], message=self.row['message'])  # подготавливаем месаг
            if self.debug:
                print mes, "\n"
                print "to_log(): try execute token_file() to get sequence_token\n"
            sequence_token = self.token_file()
            if sequence_token == "":
                if self.debug:
                    print "to_log(): if sequence_token is empty, set fake sequence_token: 000000000 \n"
                sequence_token = str("000000000")
            if self.debug:
                print "to_log(): query to AWS - put_log_events() with variables:\n"
                print self.row['group'], self.row['stream'], [mes], str(sequence_token), "\n"
            response = self.logs.put_log_events(self.row['group'], self.row['stream'], [mes], str(sequence_token))  # отправляем месаг
            if self.debug:
                print "to_log(): response from AWS:\n"
                print response, "\n"
            if self.debug:
                print "to_log(): execute token_file() with variables:\n"
                print self.row['group'], self.row['stream'], response['nextSequenceToken'], "\n"
            self.token_file(response['nextSequenceToken'])
            if self.debug:
                print "end to_log()\n"
        except boto.logs.exceptions.DataAlreadyAcceptedException as e:
            if self.debug:
                print "to_log(): except DataAlreadyAcceptedException:\n"
                print e, "\n"
                print "exit ..\n"
            raise
        except boto.logs.exceptions.InvalidParameterException as e:
            if self.debug:
                print "to_log(): except InvalidParameterException:\n"
                print e, "\n"
                print "exit ..\n"
            raise
        except boto.logs.exceptions.InvalidSequenceTokenException as e:
            if self.debug:
                print "to_log(): except InvalidSequenceTokenException:\n"
                print e, "\n"
                print "try execute token_file() with variables and write to file new sequence_token:\n"
                print self.row['group'], self.row['stream'], e.body['expectedSequenceToken'], "\n"
            self.token_file(e.body['expectedSequenceToken'])
            if self.debug:
                print "to_log(): retry to_log() with fail flag:\n"
            if int(fail) <= int(self.config['fail_count']):
                fail += 1
                if self.debug:
                    print self.row['group'], self.row['stream'], self.row['timestamp'], self.row['message'], fail, "\n"
                self.to_log(fail)
            else:
                if self.debug:
                    print "to_log(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise
        except boto.logs.exceptions.LimitExceededException as e:
            if self.debug:
                print "to_log(): except LimitExceededException:\n"
                print e, "\n"
                print "exit ..\n"
            raise
        except boto.logs.exceptions.OperationAbortedException as e:
            if self.debug:
                print "to_log(): except OperationAbortedException:\n"
                print e, "\n"
                print "to_log(): retry to_log() with fail flag:\n"
            if int(fail) <= int(config['fail_count']):
                fail += 1
                if self.debug:
                    print self.row['group'], self.row['stream'], self.row['timestamp'], self.row['message'], fail, "\n"
                self.to_log(fail)
            else:
                if self.debug:
                    print "to_log(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise
        except boto.logs.exceptions.ResourceAlreadyExistsException as e:
            if self.debug:
                print "to_log(): except ResourceAlreadyExistsException:\n"
                print e, "\n"
                print "exit ..\n"
            raise
        except boto.logs.exceptions.ResourceInUseException as e:
            if self.debug:
                print "to_log(): except ResourceInUseException:\n"
                print e, "\n"
                print "exit ..\n"
            raise
        except boto.logs.exceptions.ResourceNotFoundException as e:
            if self.debug:
                print "to_log(): except ResourceNotFoundException:\n"
                print e, "\n"
                print "try execute check_group_stream() with variables:\n"
                print self.row['group'], self.row['stream'], "\n"
            self.check_group_stream()
            if int(fail) <= int(self.config['fail_count']):
                fail += 1
                if self.debug:
                    print "to_log(): retry to_log() with fail flag:\n"
                self.to_log(fail)
            else:
                if self.debug:
                    print "to_log(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise
        except boto.logs.exceptions.ServiceUnavailableException as e:
            if self.debug:
                print "to_log(): except ServiceUnavailableException:\n"
                print e, "\n"
                print "try sleep ", self.sleep_time(fail), " seconds ...\n"
                time.sleep(self.sleep_time(fail))
            if int(fail) <= int(self.config['fail_count']):
                if self.debug:
                    print "to_log(): retry to_log() with fail flag:\n"
                fail += 1
                self.to_log(fail)
            else:
                if self.debug:
                    print "to_log(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise
        except boto.exception.JSONResponseError as e:  # обрабатываем исключение ошибочного запроса
            if self.debug:
                print "to_log(): except JSONResponseError:\n"
                print e, "\n"
            if str(e.message) == str("Rate exceeded") and int(fail) <= int(config['fail_count']):
                if self.debug:
                    print "to_log(): catch exception 'Rate exceeded'\n"
                    print "try sleep ", self.sleep_time(fail), " seconds...\n"
                time.sleep(self.sleep_time(fail))
                if self.debug:
                    print "to_log(): retry to_log() with fail flag:\n"
                fail += 1
                self.to_log(fail)
            else:
                if self.debug:
                    print "to_log(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise

    def token_file(self, sequence_token=''):
        if self.debug:
            print "token_file(): calculate token files path:\n"
        patch = '/var/awslogs/collector/tokens/' + self.row['group'] + '/'  # формируем имя папки для сохранения
        if self.debug:
            print patch, "\n"
            print "token_file(): calculate filename:\n"
        stream = self.row['stream'].replace("/", "_")
        filename = patch + stream + '.token'  # имя файла
        filename = filename.replace("//", "/")
        if self.debug:
            print filename, "\n"
        if sequence_token == '':
            if os.path.isfile(filename) and os.access(filename, os.R_OK):
                if self.debug:
                    print "token_file(): get sequence_token from file:\n"
                f = open(filename, 'r')
                sequence_token = f.read()
                f.close()
                if self.debug:
                    print sequence_token, "\n"
        else:
            if self.debug:
                print "token_file(): write sequence_token to file:\n"
                print sequence_token, "\n"
            if not os.path.exists(patch):  # проверяем существует ли папка если нет, то создаем
                os.makedirs(patch)
            f = open(filename, 'w')
            f.write(str(sequence_token))  # пишем nextSequenceToken в файл
            f.close()
        if self.debug:
            print "end token_file(): return sequence_token:\n"
            print sequence_token, "\n"
        return sequence_token


    def check_group_stream(self, fail=0):
        """
            Производим проверку на существование группы и стрима в CloudWatch self.logs
             если их нет, то создаем.
        """
        try:
            if self.debug:
                print "check_group_stream(): query to AWS - describe_log_groups()\n"
            response = self.logs.describe_log_groups(log_group_name_prefix=self.row['group'], limit=1)  # получаем список групп соответствующий нашей группе
            if self.debug:
                print "check_group_stream(): response from AWS:\n"
                print response, "\n"
            if not response['logGroups'] or len(response['logGroups']) == 0 \
                    or response['logGroups'][0]['logGroupName'] != self.row['group']:  # проверяем на присутствие ключа logGroups, присутствие группы, ну и немного параннойи
                try:
                    if self.debug:
                        print "check_group_stream(): query to AWS try create_log_group:\n"
                        print self.row['group'], "\n"
                    res = self.logs.create_log_group(self.row['group'])
                    if self.debug:
                        print "check_group_stream(): response from AWS:\n"
                        print res, "\n"
                except boto.logs.exceptions.ResourceAlreadyExistsException:  # обработка исключения нашей паранойи
                    if self.debug:
                        print "check_group_stream(): except ResourceAlreadyExistsException, this`s normal \n"
                        print res, "\n"
                    pass
            if self.debug:
                print "check_group_stream(): query to AWS - describe_log_streams()\n"
            response = self.logs.describe_log_streams(self.row['group'], log_stream_name_prefix=self.row['stream'], limit=1)  # получаем список стримов соответствующий нашему стриму
            if self.debug:
                print "check_group_stream(): response from AWS:\n"
                print response, "\n"
            if not response['logStreams'] or len(response['logStreams']) == 0 \
                    or response['logStreams'][0]['logStreamName'] != self.row['stream']:  # проверяем на присутствие ключа logStreams, присутствие группы, ну и немного параннойи
                try:
                    if self.debug:
                        print "check_group_stream(): query to AWS try create_log_stream:\n"
                        print self.row['group'], self.row['stream'], "\n"
                    self.logs.create_log_stream(self.row['group'], self.row['stream'])
                except boto.logs.exceptions.ResourceAlreadyExistsException:  # обработка исключения нашей паранойи
                    if self.debug:
                        print "check_group_stream(): except ResourceAlreadyExistsException, this`s normal \n"
                        print res, "\n"
                    pass
                if self.debug:
                    print "check_group_stream(): init stream \n"
                mes = dict(timestamp=self.config['microtime'], message="init stream")  # подготавливаем инициализирующий месаг
                if self.debug:
                    print mes, "\n"
                    print "check_group_stream(): query to AWS - put_log_events()\n"
                response = self.logs.put_log_events(self.row['group'], self.row['stream'], [mes])  # отправляем инициализирующий месаг
                if self.debug:
                    print "check_group_stream(): response from AWS:\n"
                    print response, "\n"
                assert isinstance(response, object)
                if self.debug:
                    print "check_group_stream(): execute token_file() with params:\n"
                    print self.row['group'], self.row['stream'], response['nextSequenceToken'], "\n"
                self.token_file(response['nextSequenceToken'])
        except boto.logs.exceptions.InvalidSequenceTokenException as e:  # обрабатываем исключение неверного SequenceToken
            if self.debug:
                print "check_group_stream(): except InvalidSequenceTokenException:\n"
                print e, "\n"
                print "check_group_stream(): try execute token_file() with params:\n"
                print self.row['group'], self.row['stream'], response['nextSequenceToken'], "\n"
            self.token_file(e.body['expectedSequenceToken'])
        except boto.exception.JSONResponseError as e:  # обрабатываем исключение ошибочного запроса
            if self.debug:
                print "check_group_stream(): except JSONResponseError:\n"
                print e, "\n"
            if str(e.message) == str("Rate exceeded") and int(fail) <= int(config['fail_count']):
                if self.debug:
                    print "check_group_stream(): catch exception 'Rate exceeded'\n"
                    print "try sleep ", self.sleep_time(fail), " seconds...\n"
                time.sleep(self.sleep_time(fail))
                if self.debug:
                    print "check_group_stream(): retry check_group_stream() with fail flag:\n"
                fail += 1
                self.check_group_stream(fail)
            else:
                if self.debug:
                    print "check_group_stream(): fail count is maximum :( print error:\n"
                    print e, "\n"
                    print "exit ..\n"
                raise

    def run(self, input_row):
        if None != input_row[1]:
            if len(str(input_row[1])) == 10:
                self.row['timestamp'] = long(round(float(input_row[1]) * 1000))
            elif len(str(input_row[1])) == 13:
                self.row['timestamp'] = long(round(float(input_row[1])))
            else:
                self.row['timestamp'] = self.config['microtime']
        else:
            self.row['timestamp'] = self.config['microtime']
        if None != input_row[2] and "" != input_row[2]:
            self.row['group'] = input_row[2].encode('utf-8', 'replace')
        else:
            self.row['group'] = "NoGroup"
        if None != input_row[3] and "" != input_row[3]:
            self.row['stream'] = input_row[3].encode('utf-8', 'replace')
        else:
            self.row['stream'] = "NoStream"
        if None != input_row[4] and "" != input_row[4]:
            self.row['message'] = input_row[4].replace("<br />", "\n").replace("&nbsp;&nbsp;&nbsp;&nbsp;", "\t").encode('utf-8', 'replace')
        else:
            self.row['message'] = "no message"
        if self.debug:
            print "main(): execute to_log() with variables:\n"
            print self.row['group'], self.row['stream'], self.row['timestamp'], self.row['message'], "\n"
        self.to_log()
        if self.debug:
            print "end run()\n"


class App(Daemon):
    def __init__(self, pidfile, stdin='/dev/null'):
        global debug
        self.debug = debug
        self.stdin = stdin
        self.pidfile = pidfile
        self.con = sqlite3.connect('/var/awslogs/collector/queue.db')
        self.cur = self.con.cursor()
        self.fex = open('/var/log/collector-exception.log', 'a')

    def run(self, rise=None):
        if self.debug:
            print "\n\n\n -----------------------------------------------------------------------------------------------------------\n"
            print "App run(): enter while\n"
        while True:
            copied = False
            try:
                self.cur = self.con.cursor()
                self.cur.execute('SELECT count(*) FROM sqlite_master WHERE type = "table" AND name = "copied"')
                (tbl_cnt,) = self.cur.fetchone()
                if 0 != tbl_cnt:
                    self.cur.execute('SELECT * FROM copied LIMIT 1')
                    row = self.cur.fetchone()
                    copied = True
                    if None == row:
                        self.cur.execute('DROP TABLE copied')
                        self.con.commit()
                else:
                    self.cur.execute('SELECT count(*) FROM sqlite_master WHERE type = "table" AND name = "queue"')
                    (cnt,) = self.cur.fetchone()
                    if 0 != cnt:
                        self.cur.execute('SELECT * FROM queue LIMIT 1')
                        row = self.cur.fetchone()
                    else:
                        time.sleep(5)
                        row = None
            except Exception, e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                mess = "\n\n" + time.strftime('%X %x %Z') + \
                       "\t EXCEPTION!!: " + repr(str(e)) + "\n" + repr(traceback.format_tb(exc_traceback)) + \
                       "\n--------------------------------------\n" + repr(row) + "\n-------------------------------------\n"
                self.fex.write(mess)
                if debug:
                    sys.exit(2)
                print mess
            if None != row:
                try:
                    ex = Worker()
                    if self.debug:
                        print "\n\n\n -----------------------------------------------------------------------------------------------------------\n"
                        print "App run(): execute Worker run() with: "+repr(row)+"\n"
                    ex.run(row)
                except Exception, e:
                    if copied:
                        self.cur.execute('DELETE FROM copied WHERE id =' + str(row[0]))
                    else:
                        self.cur.execute('DELETE FROM queue WHERE id =' + str(row[0]))
                    self.con.commit()
                    self.cur.execute('INSERT INTO queue(aws_time, aws_group, aws_stream, message) VALUES(?, ?, ?, ?) ',
                                     (row[1], row[2], row[3], row[4]))
                    self.con.commit()
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    mess = "\n\n" + time.strftime('%X %x %Z') + \
                           "\t EXCEPTION!!: " + repr(str(e)) + "\n" + repr(traceback.format_tb(exc_traceback)) + \
                           "\n--------------------------------------\n" + repr(row) + "\n-------------------------------------\n"
                    self.fex.write(mess)
                    print mess
                    if debug:
                        sys.exit(2)
                    pass
                else:
                    if copied:
                        self.cur.execute('DELETE FROM copied WHERE id =' + str(row[0]))
                    else:
                        self.cur.execute('DELETE FROM queue WHERE id =' + str(row[0]))
                    self.con.commit()
            else:
                time.sleep(1)



if __name__ == "__main__":
    daemon = App('/var/run/collector.pid')
    if len(sys.argv) == 3 or 'debug' == sys.argv[2]:
        debug = True
    if len(sys.argv) == 2 or len(sys.argv) == 3:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart|debug" % sys.argv[0]
        sys.exit(2)